#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <sys/stat.h>
#include <cmath>
#include <algorithm>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sys/resource.h>
using namespace std;

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

size_t getFileSize(int fd){
	struct stat buf;
	fstat(fd, &buf);
	return buf.st_size;
}

int main(){
	int T = 1;
	vector<string> filenames(T);
	for (int i=0;i<T;++i)
		filenames[i] = "2GB" + to_string(i+1) + ".txt";
	int blkSize = 4 * 1024;	// 4 KB

	size_t fileSize = getFileSize(open("2GB1.txt", O_RDONLY));
	int numBlock = fileSize / blkSize;
	char* buf = new char[blkSize];
	unsigned long long start, end;
	system("echo 3 > /proc/sys/vm/drop_caches");
	
	// create different processes
	int id = 0;
	for (int j=0;j<T;++j){
		int pid = fork();
		if (pid==0) {
			if (j+1==T) return 0;
			continue;
		}
		else {
			id = j;
			break;
		}
	}

	int fd = open(filenames[id].c_str(), O_RDONLY);
	int i = 0;
	
	// sequence reading	
	int loops = fileSize / blkSize;
	start = rdtscp();
	for (;i<loops;++i)
		read(fd, buf, blkSize);
	end = rdtscp();
	cout << "process " << id << " read time: " << (end-start)/loops << endl;

	// random reading
	/*
	int loops = 10000;
	int totalTime = 0;
	for (;i<loops;++i){
		int offset = rand() % numBlock * blkSize;
		start = rdtscp();
		pread(fd, buf, blkSize, offset);
		end = rdtscp();
		totalTime += (end-start);
	}
	cout << "process " << id << " read time: " << totalTime/loops << endl;
	*/	

	return 0;
}
