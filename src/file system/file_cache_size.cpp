#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <sys/stat.h>
#include <cmath>
#include <algorithm>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sys/resource.h>
using namespace std;

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

size_t getFileSize(int fd){
	struct stat buf;
	fstat(fd, &buf);
	return buf.st_size;
}

int main(){
	int T = 10;
	string filename = "5.3GB.txt";
	int bufferSize = 4 * 1024;	// 4 KB

	int fd = open(filename.c_str(), O_RDONLY);
	size_t fileSize = getFileSize(fd);
	char* buf = new char[bufferSize];
	int loops = fileSize / bufferSize;

	int i = 0;
	unsigned long long start, end;
	vector<double> sol;
	
	// read firsttime
	system("echo 3 > /proc/sys/vm/drop_caches");
	for (;i<loops;++i)
		read(fd, buf, bufferSize);
	close(fd);

	// read more times to test whether cache work
	for (int j=0;j<T;++j){
		fd = open(filename.c_str(), O_RDONLY);
		i = 0;
		start = rdtscp();
		for (;i<loops;++i)
			read(fd, buf, bufferSize);
		end = rdtscp();
		sol.push_back(end-start);
		cout << sol[sol.size()-1] << " " << sol[sol.size()-1]/loops << endl;
		close(fd);
	}

	delete []buf;
}


