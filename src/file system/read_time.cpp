#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <sys/stat.h>
#include <cmath>
#include <algorithm>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sys/resource.h>
using namespace std;

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

size_t getFileSize(int fd){
	struct stat buf;
	fstat(fd, &buf);
	return buf.st_size;
}

int main(){
	srand(time(NULL));
	int T = 10;
	string filename = "1MB.txt";
	int blkSize = 4 * 1024;	// 4 KB

	int fd = open(filename.c_str(), O_RDONLY);
	size_t fileSize = getFileSize(fd);
	char* buf = new char[blkSize];
	close(fd);
	
	unsigned long long start, end;
	vector<double> sol;

	// sequential access
	for (int j=0;j<T;++j){
		// clear cache
		system("echo 3 > /proc/sys/vm/drop_caches");
		
		fd = open(filename.c_str(), O_RDONLY);
		int i = 0;
		int loops = fileSize / blkSize;
		start = rdtscp();
		for (;i<loops;++i)
			read(fd, buf, blkSize);
		end = rdtscp();
		sol.push_back(end-start);
		cout << sol[sol.size()-1]/loops << endl;
		close(fd);

	}

	// random access
	for (int j=0;j<T;++j) {
		// clear cache
		system("echo 3 > /proc/sys/vm/drop_caches");

		fd = open(filename.c_str(), O_RDONLY);
		int i = 0;
		int numBlock = fileSize / blkSize;
		int loops = 100;
		int totalTime = 0;
		for (;i<loops;++i){
			int offset = rand() % numBlock * blkSize;
			start = rdtscp();
			pread(fd, buf, blkSize, offset);
			end = rdtscp();
			totalTime += (end-start);
		}
		cout << totalTime/loops << endl;
	}

}
