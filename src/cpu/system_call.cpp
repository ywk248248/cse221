#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}


void SystemCallOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		getppid();
	}
	end = rdtscp();
	cout << "systm call overhead " << double(end - start) / N << endl;
}


int main()
{
	for (int i = 0; i < 10; i++)
	{
		uint64_t start = rdtscp();
		uint64_t end = rdtscp();
		cout << start << " " << end << endl;
		int N = 10000;

		//system call overhead
		SystemCallOverhead();
	}
	return 0;
}
