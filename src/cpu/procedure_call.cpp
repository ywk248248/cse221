#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

void testfunction0()
{
	return;
}

void testfunction1(int arg1)
{
	return;
}

void testfunction2(int arg1, int arg2)
{
	return;
}

void testfunction3(int arg1, int arg2, int arg3)
{
	return;
}

void testfunction4(int arg1, int arg2, int arg3, int arg4)
{
	return;
}

void testfunction5(int arg1, int arg2, int arg3, int arg4, int arg5)
{
	return;
}

void testfunction6(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6)
{
	return;
}

void testfunction7(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7)
{
	return;
}

void ProcedureCallOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction0();
	}
	end = rdtscp();
	cout << "no argument procedure call " << double(end - start) / N << endl;
	//one argument
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction1(0);
	}
	end = rdtscp();
	cout << "one argument procedure call " << double(end - start) / N << endl;
	//two arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction2(0, 0);
	}
	end = rdtscp();
	cout << "two arguments procedure call " << double(end - start) / N << endl;

	//three arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction3(0, 0, 0);
	}
	end = rdtscp();
	cout << "three arguments procedure call " << double(end - start) / N << endl;

	//four arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction4(0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "four arguments procedure call " << double(end - start) / N << endl;
		
	//five arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction5(0, 0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "five arguments procedure call " << double(end - start) / N << endl;
		
	//six arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction6(0, 0, 0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "six arguments procedure call " << double(end - start) / N << endl;

	//seven arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction7(0, 0, 0, 0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "seven arguments procedure call " << double(end - start) / N << endl;
}


int main()
{
	for (int i = 0; i < 10; i++)
	{
		uint64_t start = rdtscp();
		uint64_t end = rdtscp();
		cout << start << " " << end << endl;
		int N = 10000;

		//procedure call overhead
		//no argument
		ProcedureCallOverhead();
	}
    return 0;
}
