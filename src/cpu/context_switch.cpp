#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <cmath>
#include <algorithm>
#include <pthread.h>

using namespace std;

int myPipe[2];

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

void* simpleThreadFunction(void *unused){
	uint64_t start = rdtscp();
	write(myPipe[1], &start, sizeof(start));
	pthread_exit(NULL);
}

int main(){
	int N = 10;
	int T = 10000;
	double mean = 0;
	double stdDev = 0;
	vector<double> sol(N, 0);
	
	if (pipe(myPipe)){
		cout << "pipe creation fail!" << endl;
		return 0;
	}
	
	cout << "-------Process Context Switch-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;
	mean = 0;
	stdDev =0;
	for (int i=0;i<N;++i){
		for (int it=0;it<T;++it){
			auto pid = fork();
			if (pid==0) {
				uint64_t start = rdtscp();
				write(myPipe[1], &start, sizeof(start));
				exit(0);
			}
			else {
				uint64_t start, stop;
				read(myPipe[0], &start, sizeof(start));
				stop = rdtscp();
				sol[i] += (stop - start);
				wait(NULL);
			}
		}
		mean += sol[i];
		cout << "Experiment " << i << " Total Time: " << sol[i] << endl;
	}
	mean /= N;
	for (int i=0;i<N;++i)
		stdDev += (mean-sol[i]) * (mean-sol[i]);
	stdDev = sqrt(stdDev/N);
	cout << "Total Average Time: " << mean << endl;
	cout << "Standard Deviation: " << stdDev << endl << endl;

	cout << "-------Kernel Thread Context Switch-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;
	mean = 0;
	stdDev = 0;
	sol = vector<double>(N, 0);
	vector<pthread_t> threads(T);


	for (int i=0;i<N;++i){
		uint64_t start, stop;
		for (int it=0;it<T;++it){
			pthread_create(&threads[it], NULL, simpleThreadFunction, NULL);	
			read(myPipe[0], &start, sizeof(start));
			stop = rdtscp();
			sol[i] += (stop - start);
			pthread_join(threads[i], NULL);
		}
		mean += sol[i];
		cout << "Experiment " << i << " Total Time: " << sol[i] << endl;
	}
	mean /= N;
	for (int i=0;i<N;++i)
		stdDev += (mean-sol[i]) * (mean-sol[i]);
	stdDev = sqrt(stdDev/N);
	cout << "Total Average Time: " << mean << endl;
	cout << "Standard Deviation: " << stdDev << endl;


	return 0;
}
