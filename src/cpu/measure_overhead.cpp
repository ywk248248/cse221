#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

void ForLoopOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	double sum = 0.0;
	start = rdtscp();
	for (int i = 0; i < N; i++);
	end = rdtscp();
	sum += end - start;
	cout << "forloops overhead " << (double)sum / N << endl;
}

void ReadOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	double sum = 0.0;
	for (int i = 0; i < N; i++)
	{
		start = rdtscp();
		end = rdtscp();
		sum = sum + end - start;
	}
	cout << "read overhead " << double(sum) / N << endl;
}

int main()
{
	for (int i = 0; i < 10; i++)
	{
		uint64_t start = rdtscp();
		uint64_t end = rdtscp();
		cout << start << " " << end << endl;
		int N = 10000;
		//measurment overhead
		//Forloop overhead
		ForLoopOverhead();

		//register read overhead
		ReadOverhead();
	}
	return 0;
}
