#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <cmath>
#include <algorithm>
#include <pthread.h>

using namespace std;

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

void* simpleThreadFunction(void *unused){
	pthread_exit(NULL);
}

int main(){
	int N = 10;
	int T = 10000;

	cout << "-------Process Creation-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;
	vector<double> sol(10, 0);
	double mean = 0;
	double stdDev = 0;
	for (int i=0;i<N;++i){
		uint64_t start = rdtscp();
		for (int it=0;it<T;++it){
			auto pid = fork();
			if (pid==0) exit(0);
			else wait(NULL);
		}
		uint64_t stop = rdtscp();
		sol[i] = stop - start;
		mean += sol[i];
		cout << "Experiment " << i << " Total Time: " << sol[i] << endl;
	}
	mean /= N;
	for (int i=0;i<N;++i)
		stdDev += (mean-sol[i]) * (mean-sol[i]);
	stdDev = sqrt(stdDev/N);
	cout << "Total Average Time: " << mean << endl;
	cout << "Standard Deviation: " << stdDev << endl << endl;

	cout << "-------Kernel Thread Creation-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;
	mean = 0;
	stdDev = 0;
	vector<pthread_t> threads(N);

	for (int i=0;i<N;++i){
		uint64_t start = rdtscp();
		for (int it=0;it<T;++it){
			pthread_create(&threads[i], NULL, simpleThreadFunction, NULL);	
			pthread_join(threads[i], NULL);
		}

		uint64_t stop = rdtscp();
		sol[i] = stop - start;
		mean += sol[i];
		cout << "Experiment " << i << " Total Time: " << sol[i] << endl;
	}
	mean /= N;
	for (int i=0;i<N;++i)
		stdDev += (mean-sol[i]) * (mean-sol[i]);
	stdDev = sqrt(stdDev/N);
	cout << "Total Average Time: " << mean << endl;
	cout << "Standard Deviation: " << stdDev << endl;


	return 0;
}
