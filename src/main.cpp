#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

/*uint64_t rdtsc()
{
volatile int dont_remove __attribute__((unused)); // volatile to stop optimizing
unsigned tmp;
__cpuid(0, tmp, tmp, tmp, tmp);                   // cpuid is a serialising call
dont_remove = tmp;

unsigned int lo, hi;
__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
return ((uint64_t)hi << 32) | lo;
}*/

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

void testfunction0()
{
	return;
}

void testfunction1(int arg1)
{
	return;
}

void testfunction2(int arg1, int arg2)
{
	return;
}

void testfunction3(int arg1, int arg2, int arg3)
{
	return;
}

void testfunction4(int arg1, int arg2, int arg3, int arg4)
{
	return;
}

void testfunction5(int arg1, int arg2, int arg3, int arg4, int arg5)
{
	return;
}

void testfunction6(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6)
{
	return;
}

void testfunction7(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7)
{
	return;
}

void ForLoopOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	double sum = 0.0;
	start = rdtscp();
	for (int i = 0; i < N; i++);
	end = rdtscp();
	sum += end - start;
	cout << "forloops overhead " << (double)sum / N << endl;
}

void ReadOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	double sum = 0.0;
	for (int i = 0; i < N; i++)
	{
		start = rdtscp();
		end = rdtscp();
		sum = sum + end - start;
	}
	cout << "read overhead " << double(sum) / N << endl;
}

void ProcedureCallOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction0();
	}
	end = rdtscp();
	cout << "no argument procedure call " << double(end - start) / N << endl;
	//one argument
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction1(0);
	}
	end = rdtscp();
	cout << "one argument procedure call " << double(end - start) / N << endl;
	//two arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction2(0, 0);
	}
	end = rdtscp();
	cout << "two arguments procedure call " << double(end - start) / N << endl;

	//three arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction3(0, 0, 0);
	}
	end = rdtscp();
	cout << "three arguments procedure call " << double(end - start) / N << endl;

	//four arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction4(0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "four arguments procedure call " << double(end - start) / N << endl;
		
	//five arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction5(0, 0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "five arguments procedure call " << double(end - start) / N << endl;
		
	//six arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction6(0, 0, 0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "six arguments procedure call " << double(end - start) / N << endl;

	//seven arguments
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		testfunction7(0, 0, 0, 0, 0, 0, 0);
	}
	end = rdtscp();
	cout << "seven arguments procedure call " << double(end - start) / N << endl;
}

void SystemCallOverhead()
{
	uint64_t start = rdtscp();
	uint64_t end = rdtscp();
	int N = 10000;
	start = rdtscp();
	for (int i = 0; i < N; i++)
	{
		getppid();
	}
	end = rdtscp();
	cout << "systm call overhead " << double(end - start) / N << endl;
}

void allocateArray(long long size)
{
    int sizeofarray = (size) / sizeof(int);
    int *array = (int*)malloc(sizeofarray*sizeof(int));

    for(int stride = 1; stride <= pow(2,22); stride = stride*2)  //array stride range from 4B to 4MB
    {
	    uint64_t start = rdtscp();
	    uint64_t end = rdtscp(); 
        for(int i = 0; i < sizeofarray; i++)
        {
            array[i] = (i + stride) % sizeofarray;
        }
        int index = 0;                          //start testing
        start = rdtscp();
        for(int i = 0; i < 10000; i++)
        {
            index = array[index];
        }     
        end = rdtscp();  
        double res = (double)(end-start)/10000 - 6.0824;
        cout << "stride size: " << stride << " " << "array size: " << sizeofarray << " " << res << endl;
    }
    free(array);   
}

void allocateRandom(long long size)
{
    int sizeofarray = (size) / sizeof(int);
    int *array = (int*)malloc(sizeofarray*sizeof(int));

    for(int i = 0; i < sizeofarray; i++)
    {
        array[i] = i;
    }
    //randomize array
    for(int i = 0; i < sizeofarray; i++)
    {
        int m = rand()%sizeofarray;
        int n = rand()%sizeofarray;
        
        int tmp = array[m];
        array[m] = array[n];
        array[n] = tmp;
    }
    uint64_t start = rdtscp();
	uint64_t end = rdtscp(); 
    int index = 0;
    start = rdtscp();
    for(int i = 0; i < 10000; i++)
    {
        index = array[index];
    }
    end = rdtscp();
    double res = (double)(end-start)/10000 - 6.0824;
    cout << "array size: " << sizeofarray << " " << res << endl;
    free(array);    
}
int main()
{
	for (int i = 0; i < 10; i++)
	{
		uint64_t start = rdtscp();
		uint64_t end = rdtscp();
		cout << start << " " << end << endl;
		int N = 10000;
		//measurment overhead
		//Forloop overhead
		ForLoopOverhead();

		//register read overhead
		ReadOverhead();

		//procedure call overhead
		//no argument
		ProcedureCallOverhead();

		//system call overhead
		SystemCallOverhead();
	}

	//RAM access overhead
    //fixed array stride
	for(int i = 12; i <= 28; i++)         //size range from 4KB to 256MB
	{
		allocateArray(pow(2,i));
	}
    //random array
    srand(time(NULL));       //set random seed
    for(int i = 12; i <= 28; i++)
    {
        allocateRandom(pow(2,i));
    }
	return 0;
}
