#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
using namespace std;

/*uint64_t rdtsc()
{
volatile int dont_remove __attribute__((unused)); // volatile to stop optimizing
unsigned tmp;
__cpuid(0, tmp, tmp, tmp, tmp);                   // cpuid is a serialising call
dont_remove = tmp;

unsigned int lo, hi;
__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
return ((uint64_t)hi << 32) | lo;
}*/

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

void allocateArray(long long size)
{
    int sizeofarray = (size) / sizeof(int);
    int *array = (int*)malloc(sizeofarray*sizeof(int));

    for(int stride = 1; stride <= pow(2,22); stride = stride*2)  //array stride range from 4B to 4MB
    {
	    uint64_t start = rdtscp();
	    uint64_t end = rdtscp(); 
        for(int i = 0; i < sizeofarray; i++)
        {
            array[i] = (i + stride) % sizeofarray;
        }
        int index = 0;                          //start testing
        start = rdtscp();
        for(int i = 0; i < 10000; i++)
        {
            index = array[index];
        }     
        end = rdtscp();  
        double res = (double)(end-start)/10000 - 6.0824;
        cout << "stride size: " << stride << " " << "array size: " << sizeofarray << " " << res << endl;
    }
    free(array);   
}

void allocateRandom(long long size)
{
    int sizeofarray = (size) / sizeof(int);
    int *array = (int*)malloc(sizeofarray*sizeof(int));

    for(int i = 0; i < sizeofarray; i++)
    {
        array[i] = i;
    }
    //randomize array
    for(int i = 0; i < sizeofarray; i++)
    {
        int m = rand()%sizeofarray;
        int n = rand()%sizeofarray;
        
        int tmp = array[m];
        array[m] = array[n];
        array[n] = tmp;
    }
    uint64_t start = rdtscp();
	uint64_t end = rdtscp(); 
    int index = 0;
    start = rdtscp();
    for(int i = 0; i < 10000; i++)
    {
        index = array[index];
    }
    end = rdtscp();
    double res = (double)(end-start)/10000 - 6.0824;
    cout << "array size: " << sizeofarray << " " << res << endl;
    free(array);    
}
int main()
{
	//RAM access overhead
    //fixed array stride
	for(int i = 12; i <= 28; i++)         //size range from 4KB to 256MB
	{
		allocateArray(pow(2,i));
	}
    //random array
    srand(time(NULL));       //set random seed
    for(int i = 12; i <= 28; i++)
    {
        allocateRandom(pow(2,i));
    }
	return 0;
}
