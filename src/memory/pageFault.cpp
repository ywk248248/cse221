#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <cmath>
#include <algorithm>
#include <fcntl.h>
#include <sys/mman.h>

using namespace std;

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

int main(){
	int N = 1;
	int T = 100;
	int shard = 8 * 1024 * 1024;
	// T * shard < fileSize
	unsigned long long fileSize = pow(2, 30);
	fileSize *= 2;

	cout << "-------Page Fault-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;
	
	int fDes = open("file.txt", O_RDONLY);
	if (fDes == -1) {
		cout << "FILE NOT FOUND!" << endl;
		return 0;
	}
	
	char* fileMap = (char*) mmap(NULL, fileSize, PROT_READ, MAP_SHARED, fDes, 0);
	long pageSize = sysconf(_SC_PAGESIZE);
	cout << "Page Size in Bytes: " << pageSize << endl; 
	
	vector<double> sol(10, 0);
	double mean = 0;
	double stdDev = 0;
	long long sum = 0;

	for (int i=0;i<N;++i){
		char character;
		system("echo 3 > /proc/sys/vm/drop_caches");
		uint64_t start = rdtscp();
		for (int it=0;it<T;++it){
			character = fileMap[(it+1) * shard];
			sum += character;
		}
		uint64_t stop = rdtscp();

		sol[i] = stop - start;
		mean += sol[i];
		cout << "Experiment " << i << " Total Time: " << sol[i] << endl;
	}
	mean /= N;
	for (int i=0;i<N;++i)
		stdDev += (mean-sol[i]) * (mean-sol[i]);
	stdDev = sqrt(stdDev/N);
	cout << "Total Average Time: " << mean << endl;
	cout << "Standard Deviation: " << stdDev << endl << endl;

	return 0;
}
