/*************************************************************************
    > File Name: helper.cpp
    > Author: ma6174
    > Mail: ma6174@163.com 
    > Created Time: Mon 14 Nov 2016 12:52:51 PM PST
 ************************************************************************/

#include <iostream>
#include <fstream>
#include <random>
#include <string>
#include <cstring>

using namespace std;

int main(){
	srand(time(NULL));
	ofstream ofs;
	for (int t=0; t<8;++t){
	ofs.open("4MB" + to_string(t+1) + ".txt", ofstream::out);
	for (long long i=0;i<1024.0*1024*4;++i)
		ofs << (char) ('A' + rand()%60);
	ofs.close();
	ofs.clear();
	}

	return 0;
}
