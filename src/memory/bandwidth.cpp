#include <stdint.h>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <sys/wait.h>
#include <cmath>
#include <algorithm>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sys/resource.h>
using namespace std;

static inline uint64_t rdtscp(){
	uint64_t lo, hi;
	__asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

int main(){
	double freq = 2.5e9;
	int T = 1000000;
	int half = T / 2;

	// initialize memory
	long long *A = (long long*) malloc(T * sizeof(long long));
	
	cout << "-------Read Bandwidth-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;

	long long  tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10;
	long long  tmp11, tmp12, tmp13, tmp14, tmp15, tmp16, tmp17, tmp18, tmp19, tmp20;
	int i = 0;
	uint64_t start = rdtscp();
	for (;i<half;i+=10){
		tmp1 = A[i];
		tmp2 = A[half+i];
		tmp3 = A[i+1];
		tmp4 = A[half+i+1];
		tmp5 = A[i+2];
		tmp6 = A[half+i+2];
		tmp7 = A[i+3];
		tmp8 = A[half+i+3];
		tmp9 = A[i+4];
		tmp10 = A[half+i+4];
		tmp11 = A[i+5];
		tmp12 = A[half+i+5];
		tmp13 = A[i+6];
		tmp14  = A[half+i+6];
		tmp15 = A[i+7];
		tmp16 = A[half+i+7];
		tmp17 = A[i+8];
		tmp18 = A[half+i+8];
		tmp19 = A[i+9];
		tmp20 = A[half+i+9];
	}
	uint64_t end = rdtscp();
	cout << "Bandwidth = " << freq / (end-start) * T * sizeof(long long) / 1e9 << " GB/s" << endl;

	cout << "-------Read Bandwidth-------" << endl;
	cout << "Iterations of Each Experiment: " << T << endl;
	i = 0;
	start = rdtscp();
	for (;i<half;i+=10){
		A[i] = 1;
		A[half+i] = 2;
		A[i+1] = 3;
		A[half+i+1] = 4;
		A[i+2] = 5;
		A[half+i+2] = 6;
		A[i+3] = 7;
		A[half+i+3] = 8;
		A[i+4] = 9;
		A[half+i+4] = 10;
		A[i+5] = 11;
		A[half+i+5] = 12;
		A[i+6] = 13;
		A[half+i+6] = 14;
		A[i+7] = 15;
		A[half+i+7] = 16;
		A[i+8] = 17;
		A[half+i+8] = 18;
		A[i+9] = 19;
		A[half+i+9] = 20;
	}
	end = rdtscp();
	cout << "Bandwidth = " << freq / (end-start) * T * sizeof(long long) / 1e9 << " GB/s" << endl;


	return 0;
}



	/*
	int stackSize = 1024*1024*1024; // 1GB stack size
	rlimit r1;
	r1.rlim_cur = stackSize;
	int result = setrlimit(RLIMIT_STACK, &r1);
	if (result != 0){
		cout << "error in setting stack size!" << endl;
		return 0;
	}
	*/

