#include<stdio.h> 
#include<string.h>    
#include<sys/socket.h>    
#include<arpa/inet.h> 

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

int main(int argc , char *argv[])
{
    int sock;
    struct sockaddr_in server;

    if (argc < 2) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       return 1;
    }
     
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock < -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons( 8888 );
 
    //Connect to remote server
	uint64_t start, end;
	int i = 0;
	start = rdtscp();
	connect(sock , (struct sockaddr *)&server , sizeof(server));
    end = rdtscp();
	printf("Connect time: %f\n", (double)(end-start));
     
    //keep communicating with server  
    //char c;
	//send(sock, &c, sizeof(c), 0);
	//recv(sock, &c, sizeof(c), 0);
    
    close(sock);
    return 0;
}
