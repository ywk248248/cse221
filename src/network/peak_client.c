#include<stdio.h> 
#include<string.h>    
#include<sys/socket.h>    
#include<arpa/inet.h> 

static inline uint64_t rdtscp()
{
	uint64_t lo, hi;
	__asm__ __volatile__ ("rdtscp" : "=a"(lo), "=d"(hi) : : "%ecx");
	return (hi << 32) + lo;
}

int main(int argc , char *argv[])
{
    int sock;
    struct sockaddr_in server;
    ssize_t bytes, t_bytes;
    char buffer[2000];

    if (argc < 2) {
       fprintf(stderr,"usage %s hostname\n", argv[0]);
       return 1;
    }
     
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock < -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons( 8888 );
 
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
     
    puts("Connected\n");
     
    //keep communicating with server  
    char c;
	uint64_t start, end, t_start, t_end;

	t_start = rdtscp();
    start = rdtscp();
    
    
    while((bytes = recv(sock, buffer, 2000, MSG_WAITALL)))
    {
        end = rdtscp(); 
        double tmp_res = (double)(end-start);
        start = end;
        t_bytes = t_bytes + bytes;
        //printf("bytes: %llu, time: %f\n", (unsigned long long) bytes, tmp_res);
    }

    t_end = rdtscp();
    double res = (double)(t_end-t_start);
	printf("total bytes: %llu, time: %f\n",(unsigned long long) t_bytes, res);
    
    close(sock);
    return 0;
}
